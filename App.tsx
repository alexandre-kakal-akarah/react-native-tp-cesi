import "react-native-gesture-handler";
import { StatusBar } from "expo-status-bar";
import { SafeAreaView, Platform, StyleSheet } from "react-native";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import MainNavigation from "./src/navigation/navigation";
import Pedometer from "./src/screens/Pedometer/Pedometer";

export default function App() {
	const queryClient = new QueryClient();

	return (
		<QueryClientProvider client={queryClient}>
			{/* <Pedometer /> */}
			<MainNavigation />
		</QueryClientProvider>
	);
}

const styles = StyleSheet.create({
	androidContainer: {
		paddingTop: 50,
	},
});
