import AsyncStorage from "@react-native-async-storage/async-storage";
import { create } from "zustand";
import { persist, createJSONStorage } from "zustand/middleware";
import { tProduct } from "../types/products";

interface favoritesStore {
	favorites: tProduct[];
	toggleFavorite: (product: tProduct) => void;
}

export const useFavoriteStore = create<favoritesStore>()(
	persist(
		(set, get) => ({
			favorites: [],
			toggleFavorite: (product) => {
				const favs = get().favorites;
				const isLiked = favs.find((fav) => fav.id === product.id)
					? true
					: false;
				console.log(isLiked, "in store");
				if (isLiked) {
					set({ favorites: favs.filter((fav) => fav.id !== product.id) });
				} else {
					set({ favorites: [...favs, product] });
				}
			},
		}),
		{
			name: "favorites-storage",
			storage: createJSONStorage(() => AsyncStorage),
		}
	)
);
