import { create } from "zustand";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { tProduct } from "../types/products";
import { persist, createJSONStorage } from "zustand/middleware";

export interface tCartItem extends tProduct {
	quantity: number;
}

interface CartStore {
	cartData: tCartItem[];
	setCartData: (item: tCartItem) => void;
	emptyCartData: () => void;
}

export const useCartStore = create<CartStore>()(
	persist(
		(set, get) => ({
			cartData: [],

			setCartData: (item) => {
				// check if the product is already in the cart
				const cart = get().cartData;
				const productFromCart = cart.find(
					(cartProduct) => cartProduct.id === item.id
				);
				if (!productFromCart) {
					// add product to cart
					set({ cartData: [...cart, item] });
				}
				if (productFromCart && item?.quantity === 0) {
					// delete product from cart
					const updatedCart = cart.filter(
						(cartProduct) => productFromCart.id !== cartProduct.id
					);
					return set({ cartData: updatedCart });
				}

				if (productFromCart) {
					// update the product qty
					const updatedCart = cart.map((cartProduct) =>
						cartProduct.id === item.id
							? {
									...cartProduct,
									quantity: item.quantity,
							  }
							: cartProduct
					);
					set({ cartData: updatedCart });
				}
			},

			emptyCartData() {
				set({ cartData: [] });
			},
		}),
		{
			name: "cart-storage",
			storage: createJSONStorage(() => AsyncStorage),
		}
	)
);
