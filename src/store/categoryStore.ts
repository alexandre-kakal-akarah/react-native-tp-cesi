import { create } from "zustand";

interface CategoryStore {
	category: string | null;
	setCategory: (search: string) => void;
}

export const useCategoryStore = create<CategoryStore>((set) => ({
	category: null,
	setCategory: (category) => set({ category }),
}));
