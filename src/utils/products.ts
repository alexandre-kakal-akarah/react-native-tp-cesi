import "react-native-get-random-values";
import { tProduct } from "../types/products";
import { tCartItem } from "../store/cartStore";
import { v4 as uuidv4 } from "uuid";

export const getAllProducts = async (category: string | null) => {
	try {
		let url = "https://fakestoreapi.com/products";
		if (category && category !== "") {
			url += `/category/${category}`;
		}
		const res = await fetch(url);
		const data = await res.json();
		return data as tProduct[];
	} catch (error) {
		console.log(error);
	}
};

export const getProductById = async (id: string) => {
	try {
		const res = await fetch(`https://fakestoreapi.com/products/${id}`);
		const data = await res.json();
		return data;
	} catch (error) {
		console.log(error);
	}
};

export const purchase = async (cartData: tCartItem[]) => {
	try {
		const myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		myHeaders.append(
			"Authorization",
			"Bearer patlu5KoL3HvqS0HX.6638413bb0a360466d7f8564d34300c7f041dd134f54b1e29ec20a342414738c"
		);

		const newOrderId = uuidv4().toString();

		const records = cartData.map((item) => ({
			fields: {
				orderId: newOrderId,
				id: item.id.toString(),
				title: item.title,
				description: item.description,
				category: item.category,
				image: item.image,
				price: item.price,
				quantity: item.quantity,
			},
		}));

		const raw = JSON.stringify({ records });

		console.log(raw, "row");

		const res = await fetch(
			"https://api.airtable.com/v0/app64duqmRZKLrXsf/orders",
			{
				method: "POST",
				headers: myHeaders,
				body: raw,
			}
		);

		console.log(JSON.stringify(res));

		return res;
	} catch (error) {
		console.log(error);
	}
};
