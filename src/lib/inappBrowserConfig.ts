import { Alert, Linking, Platform, Dimensions, StatusBar } from "react-native";
import { InAppBrowser } from "react-native-inappbrowser-reborn";

const sleep = (timeout: number) =>
	new Promise<void>((resolve) => setTimeout(resolve, timeout));

const { width, height } = Dimensions.get("window");

export const browserConfig = {
	// iOS Properties
	dismissButtonStyle: "close",
	preferredBarTintColor: "#000",
	preferredControlTintColor: "white",
	readerMode: false,

	modalPresentationStyle: "formSheet",
	modalTransitionStyle: "flipHorizontal",
	modalEnabled: true,
	enableBarCollapsing: true,
	formSheetPreferredContentSize: {
		width: width - width / 6,
		height: height - height / 6,
	},
	// Android Properties
	showTitle: true,
	toolbarColor: "#000",
	secondaryToolbarColor: "black",
	navigationBarColor: "black",
	navigationBarDividerColor: "white",
	enableUrlBarHiding: true,
	enableDefaultShare: true,
	forceCloseOnRedirection: false,
	headers: {
		"my-custom-header": "my custom header value",
	},
	hasBackButton: true,
	browserPackage: undefined,
	showInRecents: true,
	includeReferrer: true,
};

export const openLink = async (url: string, animated = true) => {
	try {
		if (await InAppBrowser.isAvailable()) {
			// A delay to change the StatusBar when the browser is opened
			const delay = animated && Platform.OS === "ios" ? 400 : 0;
			setTimeout(() => StatusBar.setBarStyle("light-content"), delay);

			await InAppBrowser.open(url, browserConfig);
			// A delay to show an alert when the browser is closed
			await sleep(800);
		} else {
			Linking.openURL(url);
		}
	} catch (error) {
		await sleep(50);
		const errorMessage = (error as Error).message || (error as string);
		Alert.alert(errorMessage);
	} finally {
		// Restore the previous StatusBar of the App
		//StatusBar.setBarStyle(statusBarStyle);
	}
};

export const getDeepLink = (path = "") => {
	const scheme = "my-demo";
	const prefix =
		Platform.OS === "android" ? `${scheme}://demo/` : `${scheme}://`;
	return prefix + path;
};
