import React, { useState } from "react";
import { View, Text, Image } from "react-native";
import Checkbox from "../ui/Checkbox";
import { tCartItem } from "../../store/cartStore";
import ProductCount from "../ProductCount/ProductCount";
import { useCartStore } from "../../store/cartStore";

type CartItemProps = {
	product: tCartItem;
};

const CartItem = ({ product }: CartItemProps) => {
	// console.log("product", product);
	const { setCartData } = useCartStore();
	const [isChecked, setChecked] = useState(true);
	const [count, setCount] = useState(product.quantity);

	const updateCartQty = (newCount: number) => {
		console.log("newCount", newCount);
		const newCartData = {
			...product,
			quantity: newCount,
		};
		setCartData(newCartData);
	};

	return (
		<View
			style={{
				flexDirection: "row",
				alignItems: "center",
				paddingHorizontal: 20,
				gap: 10,
				height: 100,
			}}
		>
			<Checkbox isActive={isChecked} setIsActive={setChecked} />
			<View
				style={{
					flexDirection: "row",
					alignItems: "center",
					marginLeft: "auto",
					backgroundColor: "#F1F6FA",
					flex: 1,
					padding: 15,
					borderRadius: 10,
					gap: 10,
				}}
			>
				<View style={{ backgroundColor: "#fff", padding: 5, borderRadius: 5 }}>
					<Image
						source={{ uri: product.image }}
						style={{ height: "100%", aspectRatio: 1 / 1, objectFit: "contain" }}
					/>
				</View>
				<View
					style={{
						flex: 1,
					}}
				>
					<Text
						numberOfLines={2}
						style={{ fontWeight: "500", marginBottom: 5 }}
					>
						{product.title}
					</Text>
					<Text>${product.price}</Text>
				</View>
				<View style={{ marginLeft: "auto" }}>
					<ProductCount
						count={count}
						setCount={setCount}
						countMin={0}
						padding={5}
						size={15}
						updateCartQty={updateCartQty}
					/>
				</View>
			</View>
		</View>
	);
};

export default CartItem;
