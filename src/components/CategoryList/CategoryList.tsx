import { FlatList } from "react-native";
import React from "react";
import CategoryItem from "../CategoryItem/CategoryItem";

const CategoryList = () => {
	const categories = [
		"electronics",
		"jewelery",
		"men's clothing",
		"women's clothing",
	];

	return (
		<FlatList
			renderItem={RenderCategoryItem}
			data={categories}
			horizontal
			style={{
				marginBottom: 20,
				height: 40,
				maxHeight: 40,
			}}
			contentContainerStyle={{
				alignItems: "flex-start",
				height: 40,
			}}
		/>
	);
};

const RenderCategoryItem = ({ item }: { item: string }) => {
	return <CategoryItem name={item} />;
};

export default CategoryList;
