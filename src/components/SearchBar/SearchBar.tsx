import { View, TextInput } from "react-native";
import React from "react";
import styles from "./SearchBar.style";
import { useSearchStore } from "../../store/searchStore";

const SearchBar = () => {
	const { setSearch } = useSearchStore();
	return (
		<View style={{ paddingHorizontal: 20, marginTop: 15, marginBottom: 20 }}>
			<TextInput
				placeholder="Search products..."
				style={{
					backgroundColor: "#F1F6FA",
					paddingHorizontal: 20,
					paddingVertical: 10,
					borderRadius: 10,
				}}
				onChangeText={setSearch}
			/>
		</View>
	);
};

export default SearchBar;
