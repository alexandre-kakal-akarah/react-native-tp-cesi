import { View, Text } from "react-native";
import React from "react";

type ProductTagProps = {
	name: string;
};

const ProductTag = ({ name }: ProductTagProps) => {
	return (
		<View
			style={{
				position: "absolute",
				backgroundColor: "#3E5259",
				bottom: 5,
				right: 5,
				paddingVertical: 4,
				paddingHorizontal: 10,
				borderRadius: 40,
			}}
		>
			<Text style={{ fontSize: 10, color: "#F1F6FA" }}>{name}</Text>
		</View>
	);
};

export default ProductTag;
