import { View, Text, StyleSheet } from "react-native";
import React from "react";
import { Divider } from "@rneui/themed";

type PostProps = {
	title: string;
	content: string;
};

const Post = ({ title, content }: PostProps) => {
	return (
		<View>
			<Text style={styles.title}>{title}</Text>
			<Divider color="#000" />
			<Text style={styles.content}>{content}</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	title: {
		fontSize: 20,
		paddingBottom: 10,
		paddingTop: 30,
	},
	content: {
		marginTop: 10,
	},
});

export default Post;
