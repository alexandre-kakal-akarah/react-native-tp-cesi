import { View, Text, Pressable } from "react-native";
import React from "react";
import Ionicons from "@expo/vector-icons/Ionicons";

type ProductCountProps = {
	count: number;
	countMin?: number;
	padding?: number;
	size?: number;
	setCount: React.Dispatch<React.SetStateAction<number>>;
	updateCartQty?: (newCount: number) => void;
};

const ProductCount = ({
	count,
	countMin = 1,
	size = 20,
	padding = 8,
	setCount,
	updateCartQty = () => {},
}: ProductCountProps) => {
	return (
		<View
			style={{
				flexDirection: "row",
				borderRadius: 60,
				backgroundColor: "#F1F6FA",
				gap: 10,
				alignItems: "center",
			}}
		>
			<Pressable
				style={{
					backgroundColor: "#EFEDE1",
					padding: padding,
					borderRadius: 60,
				}}
				disabled={count <= countMin}
				onPress={() => {
					if (count > countMin) {
						// it triggers only when updateCartQty is passed as a prop
						updateCartQty(count - 1);
						setCount(count - 1);
					}
				}}
			>
				<Ionicons name="remove" size={size} color="#202F32" />
			</Pressable>
			<Text>{count}</Text>
			<Pressable
				disabled={count >= 99}
				style={{
					backgroundColor: "#202F32",
					padding: padding,
					borderRadius: 60,
				}}
				onPress={() => {
					if (count < 99) {
						updateCartQty(count + 1);
						setCount(count + 1);
					}
				}}
			>
				<Ionicons name="add" size={size} color="#fff" />
			</Pressable>
		</View>
	);
};

export default ProductCount;
