import { Image } from "react-native";
import React from "react";
import { useSearchStore } from "../../store/searchStore";

const RenderImage = () => {
	const { search } = useSearchStore();
	if (search === "") return null;

	return (
		<Image
			source={{ uri: `https://loremflickr.com/320/240/${search}` }}
			style={{ width: 320, height: 240 }}
		/>
	);
};

export default RenderImage;
