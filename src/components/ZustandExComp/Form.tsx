import React, { useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Input } from "@rneui/themed";
import { useSearchStore } from "../../store/searchStore";

const Form = () => {
	const { setSearch } = useSearchStore();
	const [inputValue, setInputValue] = useState("");
	return (
		<View>
			<Text
				style={{
					marginTop: 30,
					fontSize: 20,
					fontWeight: "bold",
					marginBottom: 10,
				}}
			>
				Votre mot clé
			</Text>
			<Input placeholder="Rechercher" onChangeText={setInputValue} />
			<TouchableOpacity
				onPress={() => {
					setSearch(inputValue);
				}}
				style={{
					backgroundColor: "#000",
					alignItems: "center",
					paddingVertical: 15,
					borderRadius: 5,
					marginBottom: 20,
				}}
			>
				<Text style={{ color: "#fff", fontWeight: "500" }}>
					Charger l'image
				</Text>
			</TouchableOpacity>
		</View>
	);
};

export default Form;
