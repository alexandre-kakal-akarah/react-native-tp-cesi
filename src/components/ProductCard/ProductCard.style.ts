import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	container: {
		backgroundColor: "#F1F6FA",
		height: 130,
		flex: 1,
		marginHorizontal: 20,
		flexDirection: "row",
		borderRadius: 10,
		marginBottom: 15,
	},
});

export default styles;
