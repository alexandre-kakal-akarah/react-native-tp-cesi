import { View, Text, Image, TouchableOpacity } from "react-native";
import React from "react";
import { tProduct } from "../../types/products";
import ProductTag from "../ProductTag/ProductTag";
import styles from "./ProductCard.style";
import { useNavigation } from "@react-navigation/native";

type ProductCardProps = {
	product: tProduct;
};

const ProductCard = ({ product }: ProductCardProps) => {
	const navigation = useNavigation();

	if (product.title === "blankItem") {
		return <View style={{ width: "100%", height: 150 }}></View>;
	}

	return (
		<TouchableOpacity
			activeOpacity={0.8}
			onPress={() => {
				//@ts-ignore
				navigation.navigate("ProductDetail", { product });
			}}
		>
			<View style={styles.container}>
				<View
					style={{
						width: "40%",
						padding: 15,
						backgroundColor: "#fff",
						margin: 10,
						borderRadius: 10,
						position: "relative",
					}}
				>
					<Image
						source={{ uri: product.image }}
						style={{ width: "100%", height: "100%", objectFit: "contain" }}
					/>
					<ProductTag name={product.category} />
				</View>
				<View style={{ width: "50%" }}>
					<Text
						numberOfLines={1}
						ellipsizeMode="tail"
						style={{
							fontSize: 16,
							fontWeight: "bold",
							color: "#202F32",
							marginTop: 10,
						}}
					>
						{product.title}
					</Text>
					<Text
						numberOfLines={2}
						style={{ fontSize: 12, marginTop: 5, color: "#3E5259" }}
					>
						{product.description}
					</Text>
					<Text
						style={{
							marginTop: "auto",
							marginBottom: 10,
							fontSize: 18,
							fontWeight: "bold",
						}}
					>
						${product.price}
					</Text>
				</View>
			</View>
		</TouchableOpacity>
	);
};

export default ProductCard;
