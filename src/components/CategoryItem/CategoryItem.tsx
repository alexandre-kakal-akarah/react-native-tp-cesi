import { View, Pressable, Text } from "react-native";
import React from "react";
import { useCategoryStore } from "../../store/categoryStore";
import styles from "./CategoryItem.style";

type CategoryItemProps = {
	name: string;
};

const CategoryItem = ({ name }: CategoryItemProps) => {
	const { setCategory, category } = useCategoryStore();
	return (
		<Pressable
			style={[
				styles.button,
				category === name ? styles.buttonEnable : styles.buttonDisable,
				name === "women's clothing" && {
					marginRight: 20,
				},
				name === "electronics" && {
					marginLeft: 20,
				},
			]}
			onPress={() => {
				if (category === name) {
					setCategory("");
				} else {
					setCategory(name);
				}
			}}
		>
			<View>
				<Text
					style={category === name ? { color: "#fff" } : { color: "#202F32" }}
				>
					{name}
				</Text>
			</View>
		</Pressable>
	);
};

export default CategoryItem;
