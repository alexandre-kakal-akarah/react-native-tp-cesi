import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	button: {
		borderWidth: 1,
		paddingHorizontal: 10,
		paddingVertical: 5,
		justifyContent: "center",
		borderRadius: 40,
		marginLeft: 10,
		borderColor: "#202F32",
	},
	buttonDisable: {
		backgroundColor: "#fff",
	},
	buttonEnable: {
		backgroundColor: "#202F32",
	},
});

export default styles;
