import { View, Text } from "react-native";
import React from "react";
import Ionicons from "@expo/vector-icons/Ionicons";

type ButtonProps = {
	title: string;
	icon: string;
};

const Button = ({ icon, title }: ButtonProps) => {
	return (
		<View style={{ alignItems: "center", gap: 3 }}>
			<View
				style={{
					backgroundColor: "#0f65fa",
					alignItems: "center",
					justifyContent: "center",
					borderRadius: 50,
					padding: 6,
				}}
			>
				{/* @ts-ignore */}
				<Ionicons name={icon} size={25} color="white" />
			</View>
			<Text>{title}</Text>
		</View>
	);
};

export default Button;
