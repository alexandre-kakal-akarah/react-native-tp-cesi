import { View, Text, Image, StyleSheet } from "react-native";
import React from "react";
import Ionicons from "@expo/vector-icons/Ionicons";
import Button from "./Button";

const buttons = [
	{
		title: "Buy",
		icon: "add",
	},
	{
		title: "Sell",
		icon: "remove",
	},
	{
		title: "Send",
		icon: "arrow-up",
	},
	{
		title: "Receive",
		icon: "arrow-down",
	},
	{
		title: "More",
		icon: "ellipsis-horizontal",
	},
];

const Balance = () => {
	return (
		<View style={styles.container}>
			<View style={{ flexDirection: "row" }}>
				<Ionicons name="menu" size={30} color="black" />
				<View
					style={{
						flexDirection: "row",
						alignItems: "center",
						gap: 3,
						backgroundColor: "rgba(15, 101, 250, 0.1)",
						paddingVertical: 5,
						paddingHorizontal: 10,
						borderRadius: 20,
						marginLeft: "auto",
					}}
				>
					<Ionicons name="gift" size={18} color="#0f65fa" />
					<Text style={{ color: "#0f65fa", fontWeight: "bold" }}>Get $10</Text>
				</View>
				<Image
					source={require("../../../assets/img/bell-regular-24.png")}
					style={{ width: 30, height: 30, marginLeft: 5 }}
				/>
			</View>
			<View
				style={{
					marginTop: 5,
					flexDirection: "row",
					alignItems: "center",
				}}
			>
				<View>
					<Text style={{ color: "grey", fontWeight: "500" }}>Your balance</Text>
					<Text style={{ fontSize: 28, fontWeight: "bold" }}>$623.23</Text>
				</View>
				<Image
					source={require(`../../../assets/img/graph6.png`)}
					style={{ width: 90, objectFit: "contain", marginLeft: "auto" }}
				/>
			</View>
			<View
				style={{
					flexDirection: "row",
					justifyContent: "space-between",
					marginBottom: 10,
				}}
			>
				{buttons.map((button, index) => (
					<Button key={index} title={button.title} icon={button.icon} />
				))}
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		paddingHorizontal: 15,
	},
});

export default Balance;
