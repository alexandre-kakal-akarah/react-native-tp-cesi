import { View, Pressable } from "react-native";
import React from "react";

type CheckboxProps = {
	isActive: boolean;
	setIsActive: React.Dispatch<React.SetStateAction<boolean>>;
};

const Checkbox = ({ isActive, setIsActive }: CheckboxProps) => {
	return (
		<Pressable
			onPress={() => setIsActive(!isActive)}
			style={{
				borderWidth: 1,
				borderColor: "#202F32",
				width: 25,
				height: 25,
				justifyContent: "center",
				alignItems: "center",
				borderRadius: 40,
			}}
		>
			{isActive && (
				<View
					style={{
						width: 15,
						height: 15,
						backgroundColor: "#202F32",
						borderRadius: 40,
					}}
				></View>
			)}
		</Pressable>
	);
};

export default Checkbox;
