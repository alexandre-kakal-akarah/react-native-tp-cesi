import Home from "../screens/Home/Home";
import { RootStackParamList } from "./types";
import Cart from "../screens/Cart/Cart";
import WishList from "../screens/WishList/WishList";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
const Tab = createBottomTabNavigator<RootStackParamList>();
import Ionicons from "@expo/vector-icons/Ionicons";
import * as Icon from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import { useCartStore } from "../store/cartStore";
import { useMemo } from "react";

const HomeTabsScreen = () => {
	const { cartData } = useCartStore();
	const cartQuantity = useMemo(() => {
		return cartData.reduce((acc, item) => {
			const result = acc + item.quantity;
			return result;
		}, 0);
	}, [cartData]);
	const navigation = useNavigation();
	return (
		<Tab.Navigator
			screenOptions={{
				headerShown: false,
				tabBarHideOnKeyboard: true,
				tabBarActiveTintColor: "#F15A29",
			}}
			backBehavior="history"
		>
			<Tab.Screen
				name="ShopTab"
				options={{
					tabBarLabel: "Shop",
					tabBarIcon: ({ focused }) => (
						<Icon.MaterialCommunityIcons
							name="store"
							size={25}
							color={focused ? "#F15A29" : "#000"}
						/>
					),
				}}
				component={Home}
			/>
			<Tab.Screen
				name="CartTab"
				options={{
					headerShown: true,
					headerLeft: () => {
						return (
							<Ionicons
								name="arrow-back"
								size={25}
								style={{ marginLeft: 20 }}
								onPress={() => navigation.goBack()}
							/>
						);
					},
					title: `My cart (${cartQuantity})`,
					headerTitleAlign: "center",
					tabBarLabel: "Cart",
					tabBarStyle: { display: "none" },
					tabBarIcon: ({ focused }) => (
						<Icon.MaterialCommunityIcons
							name="cart"
							size={25}
							color={focused ? "#F15A29" : "#000"}
						/>
					),
				}}
				component={Cart}
			/>
			<Tab.Screen
				name="UserTab"
				options={{
					tabBarLabel: "Wish list",
					tabBarIcon: ({ focused }) => (
						<Icon.MaterialCommunityIcons
							name="heart"
							size={25}
							color={focused ? "#F15A29" : "#000"}
						/>
					),
				}}
				component={WishList}
			/>
		</Tab.Navigator>
	);
};

export default HomeTabsScreen;
