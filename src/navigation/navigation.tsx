import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import HomeTabsScreen from "./homeTabs";
import { RootStackParamList } from "./types";
import ProductScreen from "../screens/Product/Product";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { BottomSheetModalProvider } from "@gorhom/bottom-sheet";
import { GestureHandlerRootView } from "react-native-gesture-handler";

const HomeStack = createNativeStackNavigator<RootStackParamList>();

const MyStack = () => {
	return (
		<HomeStack.Navigator
			screenOptions={{
				headerShown: false,
			}}
		>
			<HomeStack.Screen name="Shop" component={HomeTabsScreen} />
			<HomeStack.Screen
				name="ProductDetail"
				component={ProductScreen}
				options={{
					headerShown: true,
					headerTitleAlign: "center",
					title: "Product Detail",
				}}
			/>
		</HomeStack.Navigator>
	);
};

const navigation = () => {
	return (
		<GestureHandlerRootView style={{ flex: 1 }}>
			<BottomSheetModalProvider>
				<SafeAreaProvider>
					<NavigationContainer>
						<MyStack />
					</NavigationContainer>
				</SafeAreaProvider>
			</BottomSheetModalProvider>
		</GestureHandlerRootView>
	);
};

export default navigation;
