import type { NativeStackScreenProps } from "@react-navigation/native-stack";
import { tProduct } from "../types/products";

export type RootStackParamList = {
	ShopTab: {
		screen?: string;
		params?: tProduct;
	};
	Shop: undefined;
	ProductDetail: {
		screen?: string;
		params?: tProduct;
	};
	CartTab: {
		screen?: string;
	};
	UserTab: {
		screen?: string;
	};
};

export type Props = NativeStackScreenProps<RootStackParamList>;
