import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "../screens/Home/Home";
import ProductScreen from "../screens/Product/Product";
import { RootStackParamList } from "./types";

const HomeStack = createNativeStackNavigator<RootStackParamList>();

const HomeStackScreen = () => {
	return (
		<HomeStack.Navigator
			initialRouteName="Shop"
			screenOptions={{
				headerShown: false,
			}}
		>
			<HomeStack.Screen name="Shop" component={Home} />
			<HomeStack.Screen name="ProductDetail" component={ProductScreen} />
		</HomeStack.Navigator>
	);
};

export default HomeStackScreen;
