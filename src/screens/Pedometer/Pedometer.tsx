import { useState, useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Pedometer } from "expo-sensors";

export default function App() {
	const [isPedometerAvailable, setIsPedometerAvailable] = useState("checking");
	const [currentStepCount, setCurrentStepCount] = useState(0);

	const subscribe = async () => {
		const isAvailable = await Pedometer.isAvailableAsync();
		setIsPedometerAvailable(String(isAvailable));

		if (isAvailable) {
			const end = new Date();
			const start = new Date();
			start.setDate(end.getDate() - 1);

			return Pedometer.watchStepCount((result) => {
				setCurrentStepCount(result.steps);
			});
		}
	};

	useEffect(() => {
		const subscriptionFn = async () => {
			const subscription = await subscribe();
			return () => {
				/* subscription && */
				if (subscription) {
					subscription.remove();
				}
			};
		};
		subscriptionFn();
	}, []);

	return (
		<View style={styles.container}>
			<Text>Pedometer.isAvailableAsync(): {isPedometerAvailable}</Text>
			<Text>Walk! And watch this go up: {currentStepCount}</Text>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		marginTop: 15,
		alignItems: "center",
		justifyContent: "center",
	},
});
