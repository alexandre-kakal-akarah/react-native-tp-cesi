import React from "react";
import { View } from "react-native";
import Form from "../../components/ZustandExComp/Form";
import RenderImage from "../../components/ZustandExComp/RenderImage";

const ZustandEx = () => {
	return (
		<View style={{ marginHorizontal: 20 }}>
			<Form />
			<RenderImage />
		</View>
	);
};

export default ZustandEx;
