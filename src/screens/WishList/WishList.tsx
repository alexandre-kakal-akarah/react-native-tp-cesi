import {
	SafeAreaView,
	View,
	Text,
	Platform,
	FlatList,
	Image,
	Dimensions,
	TouchableOpacity,
} from "react-native";
import React from "react";
import styles from "../Home/Home.style";
import { useFavoriteStore } from "../../store/favoritesStore";
import Ionicons from "@expo/vector-icons/Ionicons";
import { Divider } from "@rneui/themed";

const WishList = () => {
	return (
		<SafeAreaView
			style={[
				Platform.OS === "android" && styles.androidContainer,
				styles.container,
			]}
		>
			<View
				style={{
					flexDirection: "row",
					alignItems: "center",
					marginTop: 25,
					marginBottom: 10,
					gap: 10,
				}}
			>
				<Text style={{ fontSize: 20, fontWeight: "bold", marginLeft: 20 }}>
					My wish list
				</Text>
				<Ionicons name="gift" size={24} color="black" />
			</View>
			<Divider color="#000" />
			<RenderFav />
		</SafeAreaView>
	);
};

const RenderFav = () => {
	const { favorites } = useFavoriteStore();
	const { width } = Dimensions.get("window");
	return (
		<FlatList
			data={favorites}
			keyExtractor={(item) => item.id.toString()}
			renderItem={({ item }) => (
				<TouchableOpacity
					style={{
						backgroundColor: "#F1F6FA",
						padding: 12,
						borderRadius: 10,
						flex: 1,
						marginHorizontal: 5,
						maxWidth: width / 2 - 25,
					}}
					activeOpacity={0.8}
				>
					<View
						style={{
							backgroundColor: "#fff",
							borderRadius: 10,
							padding: 25,
							position: "relative",
						}}
					>
						<Image
							source={{ uri: item.image }}
							style={{
								height: 100,
								aspectRatio: 1 / 1,
								objectFit: "contain",
							}}
						/>
					</View>
					<View
						style={{
							flexDirection: "row",
							marginTop: 20,
							alignItems: "flex-start",
						}}
					>
						<Text
							style={{
								fontWeight: "bold",
							}}
							numberOfLines={3}
							ellipsizeMode="tail"
						>
							{item.title}
						</Text>
					</View>
				</TouchableOpacity>
			)}
			numColumns={2}
			contentContainerStyle={{
				gap: 10,
				marginHorizontal: 15,
				paddingTop: 20,
				paddingBottom: 100,
			}}
			showsVerticalScrollIndicator={false}
		/>
	);
};

export default WishList;
