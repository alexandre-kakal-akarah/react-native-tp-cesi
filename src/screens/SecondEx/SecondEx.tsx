import { SafeAreaView, Text, Image, StyleSheet, View } from "react-native";
import React from "react";
import Ionicons from "@expo/vector-icons/Ionicons";

const SecondEx = () => {
	return (
		<SafeAreaView>
			<Image
				source={{ uri: "https://via.placeholder.com/500" }}
				style={{ objectFit: "cover", width: "100%", height: 300 }}
			/>
			<View style={styles.container}>
				<Text style={styles.title}>Exercice 2</Text>
				<Text style={styles.author}>By Alexandre Kakal</Text>
				<View style={{ flexDirection: "row", gap: 10, marginTop: 30 }}>
					<View
						style={{
							flexDirection: "row",
							alignItems: "center",
							gap: 5,
							borderWidth: 1,
							paddingVertical: 3,
							paddingHorizontal: 5,
						}}
					>
						<Ionicons name="thumbs-up" size={20} color="#444" />
						<Text>Likes</Text>
					</View>
					<View
						style={{
							flexDirection: "row",
							alignItems: "center",
							gap: 5,
							borderWidth: 1,
							paddingVertical: 3,
							paddingHorizontal: 5,
						}}
					>
						<Ionicons name="chatbubble" size={20} color="#444" />
						<Text>Comment</Text>
					</View>
				</View>
				<Text style={{ marginTop: 20 }}>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pretium
					massa ut massa feugiat, quis ultricies urna dignissim. Mauris mollis
					mauris et nibh semper, at auctor lorem pharetra. In fermentum maximus
					arcu, a cursus arcu pellentesque eget. Mauris quis risus at eros
					finibus finibus eget quis nulla. Nulla facilisis ipsum sed massa
					rutrum pulvinar. Mauris dignissim justo sem, eget rhoncus arcu
					fringilla quis. Proin molestie leo nec diam auctor mattis. Praesent
					vel suscipit ante. Sed.
				</Text>
			</View>
		</SafeAreaView>
	);
};

export default SecondEx;

const styles = StyleSheet.create({
	container: {
		paddingHorizontal: 20,
	},
	title: {
		marginTop: 35,
		fontSize: 20,
	},
	author: {
		marginTop: 5,
		fontSize: 13,
		color: "grey",
	},
});
