import React, { useState, useEffect } from "react";
import {
	SafeAreaView,
	Text,
	Platform,
	FlatList,
	ActivityIndicator,
	View,
} from "react-native";
import styles from "./Home.style";
import SearchBar from "../../components/SearchBar/SearchBar";
import { getAllProducts } from "../../utils/products";
import { useQuery } from "@tanstack/react-query";
import CategoryList from "../../components/CategoryList/CategoryList";
import { FlashList } from "@shopify/flash-list";
import ProductCard from "../../components/ProductCard/ProductCard";
import { tProduct } from "../../types/products";
import { useSearchStore } from "../../store/searchStore";
import { useCategoryStore } from "../../store/categoryStore";

const Home = () => {
	return (
		<SafeAreaView
			style={[
				Platform.OS === "android" && styles.androidContainer,
				styles.container,
			]}
		>
			<SearchBar />
			<CategoryList />

			<RenderProducts />
		</SafeAreaView>
	);
};

const RenderProducts = () => {
	const { search } = useSearchStore();
	const { category } = useCategoryStore();
	const [output, setOutput] = useState<tProduct[] | undefined>(undefined);

	const { data, isLoading, error, refetch, isRefetching } = useQuery({
		queryKey: ["products"],
		queryFn: async () => {
			const products = await getAllProducts(category);
			// add products to output with an extra blank item at the end
			const blankProduct: tProduct = {
				title: "blankItem",
				category: "",
				description: "",
				id: 0,
				image: "",
				price: 0,
				rating: {
					rate: 0,
					count: 0,
				},
				// ...other properties with default or empty values
			};

			if (products) {
				setOutput([...products, blankProduct]);
			}

			return products;
		},
	});

	useEffect(() => {
		refetch();
	}, [category]);

	useEffect(() => {
		if (search !== "") {
			const filteredProducts = data?.filter((product) =>
				product.title.toLowerCase().includes(search.toLowerCase())
			);
			setOutput(filteredProducts);
		} else {
			setOutput(data);
		}
	}, [search]);

	if (isLoading || isRefetching) {
		return (
			<View style={{ flex: 1 }}>
				<ActivityIndicator size={"large"} color="#202F32" />
			</View>
		);
	}

	if (error) {
		return <Text>Error</Text>;
	}

	return (
		<FlatList
			data={output}
			renderItem={({ item }) => <ProductCard product={item} />}
		/>
	);
};

export default Home;
