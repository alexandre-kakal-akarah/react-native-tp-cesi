import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	androidContainer: {
		marginTop: 40,
	},
	container: {
		paddingTop: 10,
		backgroundColor: "#F9F9F9",
		minHeight: "100%",
	},
});

export default styles;
