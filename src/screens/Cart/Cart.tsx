import React, { useCallback, useMemo, useRef } from "react";
import { SafeAreaView, FlatList, View, Text, Pressable } from "react-native";
import CartItem from "../../components/CartItem/CartItem";
import styles from "../Home/Home.style";
import { useCartStore } from "../../store/cartStore";
import BottomSheet from "@gorhom/bottom-sheet";
import { BottomSheetScrollView } from "@gorhom/bottom-sheet";
import { tCartItem } from "../../store/cartStore";
import { purchase } from "../../utils/products";
import { useNavigation } from "@react-navigation/native";
import { useMutation } from "@tanstack/react-query";

const Cart = () => {
	const navigation = useNavigation();
	const { cartData, emptyCartData } = useCartStore();
	const cartTotal = useMemo(() => {
		return cartData.reduce((acc, item) => {
			const result = acc + item.price * item.quantity;
			return Math.round(result * 100) / 100;
		}, 0);
	}, [cartData]);
	// ref
	const bottomSheetRef = useRef<BottomSheet>(null);

	// variables
	const snapPoints = useMemo(() => ["40%", "60%"], []);

	// callbacks
	const handleSheetChanges = useCallback((index: number) => {
		// console.log("handleSheetChanges", index);
	}, []);

	const { mutate: handleSubmit, isPending } = useMutation({
		mutationKey: ["purchase"],
		mutationFn: async () => {
			const res = await purchase(cartData);
			return res;
		},
		onSuccess: () => {
			emptyCartData();
			//@ts-ignore
			navigation.navigate("ShopTab");
		},
	});

	return (
		<SafeAreaView style={[styles.container]}>
			<RenderCartItem cartData={cartData} />
			<BottomSheet
				ref={bottomSheetRef}
				index={1}
				snapPoints={snapPoints}
				onChange={handleSheetChanges}
			>
				<BottomSheetScrollView
					contentContainerStyle={{
						flexGrow: 1,
						backgroundColor: "white",
						paddingHorizontal: 20,
						paddingTop: 10,
					}}
				>
					<Text style={{ fontSize: 18, fontWeight: "bold" }}>
						Selected items
					</Text>
					<RenderItems cartData={cartData} />
					<View
						style={{
							flexDirection: "row",
							marginTop: 30,
							alignItems: "center",
						}}
					>
						<Text style={{ fontWeight: "bold", fontSize: 18 }}>
							Total Payment
						</Text>
						<Text
							style={{
								marginLeft: "auto",
								color: "#FEA82C",
								fontWeight: "bold",
								fontSize: 18,
							}}
						>
							${cartTotal}
						</Text>
					</View>
					<Pressable
						style={{
							backgroundColor: "#202F32",
							paddingVertical: 10,
							borderRadius: 10,
							marginTop: 10,
							opacity: isPending || cartData.length === 0 ? 0.8 : 1,
						}}
						onPress={() => handleSubmit()}
						disabled={isPending || cartData.length === 0}
					>
						<Text
							style={{
								color: "#fff",
								textAlign: "center",
								fontWeight: "500",
								fontSize: 16,
							}}
						>
							Checkout Now
						</Text>
					</Pressable>
				</BottomSheetScrollView>
			</BottomSheet>
		</SafeAreaView>
	);
};

type Props = {
	cartData: tCartItem[];
};

const RenderCartItem = ({ cartData }: Props) => {
	return (
		<FlatList
			data={cartData}
			renderItem={({ item }) => <CartItem product={item} />}
			contentContainerStyle={{
				flexGrow: 1,
				gap: 15,
			}}
		/>
	);
};

const RenderItems = ({ cartData }: Props) => {
	return (
		<>
			{cartData.map((item, index) => {
				return (
					<View
						key={index}
						style={{
							flexDirection: "row",
							alignItems: "center",
							marginTop: 10,
						}}
					>
						<Text
							numberOfLines={1}
							ellipsizeMode="tail"
							style={{ fontWeight: "500", color: "#828B93", width: "40%" }}
						>
							{item.title}
						</Text>
						<Text style={{ marginLeft: 10, fontWeight: "bold", fontSize: 16 }}>
							x{item.quantity}
						</Text>
						<Text
							style={{
								marginLeft: "auto",
								color: "#FEA82C",
								fontWeight: "500",
								fontSize: 17,
							}}
						>
							${item.price}
						</Text>
					</View>
				);
			})}
		</>
	);
};
export default Cart;
