import { View, Text, ScrollView, StyleSheet } from "react-native";
import React from "react";
import Post from "../../components/Post/Post";

const FirstEx = () => {
	return (
		<ScrollView showsVerticalScrollIndicator={false}>
			<View style={styles.container}>
				<Text style={styles.title}>Exercice 1</Text>
				<View style={styles.postContainer}>
					{Array.from({ length: 10 }).map((_, index) => (
						<Post
							key={index}
							title={`Post ${index + 1}`}
							content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pretium massa ut massa feugiat, quis ultricies urna dignissim. Mauris mollis mauris et nibh semper, at auctor lorem pharetra. In fermentum maximus arcu, a cursus arcu pellentesque eget. Mauris quis risus at eros finibus finibus eget quis nulla."
						/>
					))}
				</View>
			</View>
		</ScrollView>
	);
};

export default FirstEx;

const styles = StyleSheet.create({
	container: {
		alignItems: "center",
		backgroundColor: "#cecece",
		borderColor: "black",
		borderWidth: 1.5,
		paddingHorizontal: 15,
	},
	postContainer: {
		paddingHorizontal: 15,
		paddingVertical: 15,
	},
	title: {
		fontSize: 18,
		paddingVertical: 10,
		borderBottomWidth: 1,
		borderBottomColor: "black",
		width: "100%",
		textAlign: "center",
	},
});
