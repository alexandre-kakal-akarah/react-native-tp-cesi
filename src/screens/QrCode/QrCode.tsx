import React, { useState, useEffect } from "react";
import {
	SafeAreaView,
	View,
	Text,
	TouchableOpacity,
	StyleSheet,
	Image,
} from "react-native";
import { BarCodeScanner } from "expo-barcode-scanner";
import { openLink } from "../../lib/inappBrowserConfig";

const QrCode = () => {
	const [hasPermission, setHasPermission] = useState<boolean | null>(null);
	const [scanStatus, setScanStatus] = useState("off");
	const [scanOutput, setScanOutput] = useState<string | null>(null);

	useEffect(() => {
		const getBarCodeScannerPermissions = async () => {
			const { status } = await BarCodeScanner.requestPermissionsAsync();
			setHasPermission(status === "granted");
		};

		getBarCodeScannerPermissions();
	}, []);

	// @ts-ignore
	const handleBarCodeScanned = ({ type, data }) => {
		setScanStatus("done");
		setScanOutput("Redirection vers : " + data);
		// open inappbrowser
		openLink(data);
	};

	const closeScan = () => {
		setScanStatus("off");
		setScanOutput(null);
	};

	return (
		<SafeAreaView style={{ height: "100%" }}>
			{/* CAMERA SECTION */}
			<View
				style={{
					height: "60%",
					width: "100%",
					position: "relative",
					overflow: "hidden",
					backgroundColor: "#000",
					justifyContent: "center",
					alignItems: "center",
				}}
			>
				{scanStatus === "off" ? (
					<Image source={require("../../../assets/img/qrcode.png")} />
				) : (
					<BarCodeScanner
						onBarCodeScanned={
							scanStatus === "off" ? undefined : handleBarCodeScanned
						}
						style={{
							...StyleSheet.absoluteFillObject,
							position: "absolute",
							transform: [{ scale: 1.5 }],
						}}
					/>
				)}
			</View>
			{/* CONTROL SECTION */}
			<View
				style={{
					alignItems: "center",
					height: "40%",
				}}
			>
				<RenderTitle scanStatus={scanStatus} />
				{scanStatus === "done" && (
					<Text style={{ fontSize: 17, fontWeight: "500", marginTop: 20 }}>
						{scanOutput}
					</Text>
				)}
				<TouchableOpacity
					style={{
						backgroundColor: "#000",
						paddingVertical: 10,
						paddingHorizontal: 20,
						borderRadius: 10,
						marginTop: "auto",
						marginBottom: 20,
					}}
					onPress={() => {
						if (scanStatus === "off") {
							setScanStatus("scanning");
						} else {
							closeScan();
						}
					}}
				>
					<Text style={{ color: "#fff", fontSize: 17, fontWeight: "bold" }}>
						{scanStatus === "off" ? "Scanner un QR Code" : "Fermer le scan"}
					</Text>
				</TouchableOpacity>
			</View>
		</SafeAreaView>
	);
};

const RenderTitle = ({ scanStatus }: { scanStatus: string }) => {
	if (scanStatus === "off") {
		return (
			<Text style={{ fontSize: 20, fontWeight: "bold", marginTop: 60 }}>
				En attente de scan....
			</Text>
		);
	}

	if (scanStatus === "scanning") {
		return (
			<Text style={{ fontSize: 20, fontWeight: "bold", marginTop: 60 }}>
				Scan en cours...
			</Text>
		);
	}

	return (
		<Text style={{ fontSize: 20, fontWeight: "bold", marginTop: 60 }}>
			Résultat du scan :
		</Text>
	);
};

export default QrCode;
