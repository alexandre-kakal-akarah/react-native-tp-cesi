import React, { useState, useEffect } from "react";
import {
	View,
	Text,
	Image,
	ScrollView,
	TouchableOpacity,
	Pressable,
} from "react-native";
import type { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RootStackParamList } from "../../navigation/types";
import ProductCount from "../../components/ProductCount/ProductCount";
import Ionicons from "@expo/vector-icons/Ionicons";
import { useFavoriteStore } from "../../store/favoritesStore";
import { useCartStore } from "../../store/cartStore";
import { useNavigation } from "@react-navigation/native";

type ProductScreenProps = NativeStackScreenProps<
	RootStackParamList,
	"ProductDetail"
>;

const ProductScreen = ({ route }: ProductScreenProps) => {
	const navigation = useNavigation();
	const { id, category, description, image, price, rating, title } =
		// @ts-ignore
		route.params.product;
	const [count, setCount] = useState(1);
	const { toggleFavorite, favorites } = useFavoriteStore();
	const { setCartData } = useCartStore();
	const isLiked = favorites.find((item) => item.id === id) ? true : false;
	const [isProductLiked, setisProductLiked] = useState(isLiked);

	useEffect(() => {
		const isProductLiked = favorites.find((item) => item.id === id)
			? true
			: false;
		setisProductLiked(isProductLiked);
	}, [favorites]);

	return (
		<View style={{ position: "relative", flex: 1 }}>
			<ScrollView
				style={{ backgroundColor: "#F9F9F9" }}
				showsVerticalScrollIndicator={false}
			>
				<View
					style={{
						backgroundColor: "#F1F6FA",
						margin: 20,
						padding: 20,
						borderRadius: 10,
					}}
				>
					<View
						style={{
							backgroundColor: "#fff",
							borderRadius: 10,
							padding: 25,
							position: "relative",
						}}
					>
						<Image
							source={{ uri: image }}
							style={{
								width: "100%",
								aspectRatio: 1 / 1,
								objectFit: "contain",
							}}
						/>
						{isProductLiked ? (
							<Pressable
								style={{
									position: "absolute",
									top: 10,
									right: 10,
									backgroundColor: "#F1F6FA",
									width: 40,
									height: 40,
									borderRadius: 100,
									justifyContent: "center",
									alignItems: "center",
								}}
								onPress={() => {
									// @ts-ignore
									toggleFavorite(route.params.product);
									console.log("pressed", isProductLiked);
								}}
							>
								<Ionicons
									name="heart"
									size={25}
									color="#202F32"
									style={{ transform: [{ translateY: 1 }] }}
								/>
							</Pressable>
						) : (
							<Pressable
								style={{
									position: "absolute",
									top: 10,
									right: 10,
									backgroundColor: "#F1F6FA",
									width: 40,
									height: 40,
									borderRadius: 100,
									justifyContent: "center",
									alignItems: "center",
								}}
								// @ts-ignore
								onPress={() => toggleFavorite(route.params.product)}
							>
								<Ionicons
									name="heart-outline"
									size={25}
									color="#202F32"
									style={{ transform: [{ translateY: 1 }] }}
								/>
							</Pressable>
						)}
					</View>
					<View
						style={{
							flexDirection: "row",
							marginTop: 20,
							alignItems: "flex-start",
						}}
					>
						<Text
							style={{
								maxWidth: "60%",
								fontSize: 18,
								fontWeight: "bold",
							}}
						>
							{title}
						</Text>
						<Text
							style={{
								marginLeft: "auto",
								fontWeight: "bold",
								backgroundColor: "#202F32",
								color: "#fff",
								paddingVertical: 6,
								paddingHorizontal: 15,
								borderRadius: 8,
							}}
						>
							${price}
						</Text>
					</View>
				</View>
				{/* CONTENT */}
				<View style={{ marginHorizontal: 20 }}>
					<Text style={{ color: "#202F32", fontSize: 18, fontWeight: "bold" }}>
						Description
					</Text>
					<Text
						style={{
							marginTop: 5,
							lineHeight: 24,
							color: "#3E5259",
							marginBottom: 20,
						}}
					>
						{description}
					</Text>
				</View>
			</ScrollView>
			{/* FOOTER */}
			<View style={{ backgroundColor: "#fff", padding: 20 }}>
				<View
					style={{
						flexDirection: "row",
						marginBottom: 20,
						alignItems: "center",
					}}
				>
					<ProductCount count={count} setCount={setCount} />
					<Text style={{ marginLeft: "auto" }}>
						<Text style={{ fontSize: 16, fontWeight: "500" }}>Total : </Text>$
						{price * count}
					</Text>
				</View>
				<TouchableOpacity
					style={{ backgroundColor: "#202F32", borderRadius: 8 }}
					activeOpacity={0.8}
					onPress={() => {
						setCartData({
							id,
							category,
							description,
							image,
							price,
							rating,
							title,
							quantity: count,
						});
						// @ts-ignore
						navigation.navigate("CartTab");
					}}
				>
					<Text
						style={{
							color: "#fff",
							textAlign: "center",
							paddingVertical: 10,
							fontWeight: "bold",
							fontSize: 16,
						}}
					>
						Add to cart
					</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
};

export default ProductScreen;
