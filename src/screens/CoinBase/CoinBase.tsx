import { SafeAreaView, View, Text, StyleSheet } from "react-native";
import React from "react";
import Balance from "../../components/Ex3Comp/Balance";
import { Divider } from "@rneui/themed";

const CoinBase = () => {
	return (
		<SafeAreaView>
			<Balance />
			<Divider color="#dedede" width={1.2} />
		</SafeAreaView>
	);
};

const styles = StyleSheet.create({});

export default CoinBase;
